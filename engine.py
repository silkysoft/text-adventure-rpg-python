# Python Text adventure Game Engine
# By SilkySoft
# Master branch of https://bitbucket.org/silkysoft/text-adventure-rpg-python/
# Alpha v0.0.2

class Player:
    def __init__(self):
        self.name = "default"
        self.hp = 0
        self.str = 0
        self.int = 0
    def setup(self, name = "Not defined", hp = 0, str = 0, int = 0):
        self.name = name
        self.hp = hp
        self.str = str
        self.int = int

class itemClass:
    def __init__(self):
        self.items = []
        self.itemdesc = {}
    def addItem(self, name, desc):
        self.name = name # name
        self.desc = desc # description

        self.items.append(self.name)
        self.itemdesc[self.name] = self.desc
    def itemExist(self, name):
        if self.name in self.items:
            return True
        else:
            return False
    def getItemDesc(self, name):
        if self.itemExist(name):
            return self.itemdesc[name]
        else:
            return False

def start(name="default"):
    print "Text Adventure Engine v0.0.2 ALPHA! - By SilkySoft"
    print "Game Engine Started!"

    # defining important variales

    gamename = name

    item = itemClass() # creating the item object

    item.addItem("item1", "A usful item no.1")
    print item.getItemDesc("item1")

# debugging code
start()
